@tag
Feature: Envio de anexos
	Pra enviar anexos para qualquer contato 
	
@tag1	
Scenario: Envio de Fotos e V�deos
	Given que eu esteja logado via web
	And exista contato
	And exista fotos e v�deos a serem enviados
	When selecionar um contato
	And clicar neste contato
	And clicar sobre o bot�o anexos
	When clicar sobre o �cone Fotos e Videos
	And escolher a foto ou video desejado
	And clicar bot�o abrir
	When clicar no bot�o enviar
	Then a foto ou v�deo � enviada

@tag2
Scenario: Envio de Documento
	Given que eu esteja logado via web
	And exista contato
	And exista documento a ser enviado
	When selecionar um contato
	And clicar neste contato
	And clicar sobre o bot�o anexos
	When clicar sobre o �cone documento
	And escolher o documento desejado
	And clicar bot�o abrir
	When clicar no bot�o enviar
	Then documento � enviado

@tag3
Scenario: Envio de Contatos
	Given que eu esteja logado via web
	And exista contatos
	When selecionar um contato
	And clicar neste contato
	And clicar sobre o bot�o anexos
	When clicar sobre o �cone contato
	And marcar o(s) contato(s) desejado(s)
	And clicar no �cone seta (enviar contatos)
	And exibir os contatos marcado(s)
	When clicar no �cone seta (enviar contatos)
	Then contato � enviado

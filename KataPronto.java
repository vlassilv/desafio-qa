package desafioConcreteKata;

public class KataPronto{
	public static long calculaPreco(String produtos) {

		long totalProdA = 0;
		long totalProdB = 0;
		long totalProdC = 0;
		long totalProdD = 0;
		
		long retorno = 0;
		
		// Verifica se a string est� preenchida
		if (produtos != null && !produtos.trim().equals("")) {

			try {

				String[] ArrayProdutos = produtos.split(",");

				int qtdProdA = 0;
				int qtdProdB = 0;

				// Verifica se h� produtos
				if (ArrayProdutos != null && ArrayProdutos.length > 0) {

					for (int i = 0; i < ArrayProdutos.length; i++) {

						String tipoPoduto = ArrayProdutos[i].trim();
						int valor = 0;

						// Verifica valor do produto
						switch (tipoPoduto) {
						case "A":
							qtdProdA++;
							if (qtdProdA == 3) {
								totalProdA += 30;
								qtdProdA = 0;
							} else {
								totalProdA += 50;
							}
							break;

						case "B":
							qtdProdB++;
							if (qtdProdB == 2) {
								totalProdB += 15;
								qtdProdB = 0;
							} else {
								totalProdB += 30;
							}
							break;

						case "C":
							totalProdC += 20;
							break;

						case "D":
							totalProdD += 15;
							break;

						default:
							break;
						}

					}

				}
			} catch (Throwable e) {
				// Erro no c�lculo do produtos
				e.printStackTrace();
			}

		}
		
		retorno = totalProdA + totalProdB + totalProdC + totalProdD;
				
		System.out.println("KataPronto :: retorno :: " + retorno);

		return retorno;

	}
}
